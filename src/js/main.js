$(document).ready(function () {

  var scrollEl = document.querySelector('.rellax');

  if (scrollEl) {
    var rellax = new Rellax('.rellax');
  }

  var swiper = new Swiper('.vacancies__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.vacancies__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      600: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 20,
        allowTouchMove: false
      }
    }
  });

  var swiper = new Swiper('.testimonials__slider', {
    pagination: {
      el: '.testimonials__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        spaceBetween: 45
      },
      1200: {
        spaceBetween: 90
      },
      1440: {
        spaceBetween: 120
      }
    }
  });
});